import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_dkms_service_enabled_and_running(host):
    service = host.service("dkms")
    assert service.is_running
    assert service.is_enabled


def test_kernel_module_present(host):
    # module should automatically be loaded by udev
    # if the required PCI card is present
    # it's not loaded in the VM as there is no PCI card
    # we just check it's present
    cmd = host.run("/usr/sbin/modinfo mrf")
    assert "mrf.ko" in cmd.stdout
