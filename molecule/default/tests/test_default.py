import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_udev_rule_exists(host):
    assert host.file("/lib/udev/rules.d/50-mrf-dkms.rules").exists


def test_mrf_group_exists(host):
    assert host.group("mrf").exists
