import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_kernel_module_present(host):
    # module should be installed
    # it is not loaded on the docker
    cmd = host.run("find /lib/modules/ -name 'mrf.ko'")
    assert "mrf.ko" in cmd.stdout
