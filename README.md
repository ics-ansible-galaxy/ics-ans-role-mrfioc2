# ics-ans-role-mrfioc2

Ansible role to install the mrfioc2 kernel module.
This role depends on the kernel-rt role.

## Role Variables

See [defaults/main.yml](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-mrfioc2
```

## License

BSD 2-clause
